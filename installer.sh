#!/bin/bash
if (( $EUID != 0 )); then
  echo "Please run as sudo"
    exit
fi
apt-get update
apt-get install python3-pip
pip3 install -r requirements.txt
