#!/bin/python3
# selenium:
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import datetime
import argparse
import sys
import time # benchmarking the speed

# start = time.perf_counter()
#finish = time.perf_counter()
#print(f'Finished in {round(finish-start,2)} seconds')

VERBOSE = 0
def verbose(text):
    if VERBOSE:
        print(text)


# Returns: Cleaned up raw response to json-format
# Params: Raw x-path response
# JSON-format:
# {
#     "monday":[
#         {
#             "roomName": [  # the list containing all the orders of the room is sorted on the time.
#                 "timeRoomIsBusy_infoOfOrder_nameOfLecture"
#             ]
#         }
#     ]
# }
def cleanRawQueryResponse(rawQueryResponse):
    days = ['monday','tuesday','wednesday','thursday','friday']
    cleanedDictOfDays = {} # A list of monday-friday

    for day in days: #iterating days in dict
        cleanedDictOfDays[day] = [] #creating lists in the new cleanedList
        orderList = rawQueryResponse[day] # getting list in dict
        for order in orderList: # iterating orders in list
            dictOfOrdersOnOneDay = {}
            verbose("Creating structure in -> \n" + order)
            orderSplitBasedOnNewLine = order.splitlines() # Split each line. Returns a list.


            ##########################################
            # Maybe not the best way to create a ton of different handlings.
            # However it seems it is no structure in the orders...
            if len(orderSplitBasedOnNewLine) == 3: 
                infoOfOrder = orderSplitBasedOnNewLine[0]
                timeRoomIsBusy = orderSplitBasedOnNewLine[1]
                roomName = orderSplitBasedOnNewLine[2]
            
            #Example: ['IMT2008 : 2_Gruppe', '12:15 - 14:00', 'G. Wangen', 'GJ-A A154']
            if len(orderSplitBasedOnNewLine) == 4:
                infoOfOrder = orderSplitBasedOnNewLine[0]
                timeRoomIsBusy = orderSplitBasedOnNewLine[1]
                nameOfLecture = orderSplitBasedOnNewLine[2]
                roomName = orderSplitBasedOnNewLine[3]
            ##########################################

            # Adding info of an event to a list:
            oneEvent = []
            oneEvent.append(timeRoomIsBusy)
            oneEvent.append(infoOfOrder)
            oneEvent.append(nameOfLecture)

            # Continue with building the JSON-file
            dictOfOrdersOnOneDay[roomName] = []
            dictOfOrdersOnOneDay[roomName].append(oneEvent)
        
        cleanedDictOfDays[day] = dictOfOrdersOnOneDay.copy()
            
    print(cleanedDictOfDays)
        # Done with all the events on one day:
        #cleanedDictOfDays[day].append("test")







# Returns: xpath webelement or Null if no results
# Params: URL (str) of website to retrieve info from-
def getAllBusyWeek(url):
    dayTranslator = { # Used for translating number in query for-loop to actual day
        2:'monday',
        3:'tuesday',
        4:'wednesday',
        5:'thursday',
        6:'friday',
    }

    #   ChromeDriver:
    #######################
    CHROMEDRIVER_PATH = "/home/aksel/Documents/rombestilling/chromedriver" # chromedriver in running dir
    options = Options()
    options.headless = True
    driver = webdriver.Chrome(CHROMEDRIVER_PATH,options=options)
    verbose("Driver getting url: " + url)
    driver.get(url)
    #######################


    dictOfEachDay = {} # A list of monday-friday
    for day in range(2,7): #Create a list inside dict:
        dictOfEachDay[dayTranslator[day]] = []

    try:
        for day in range(2,7):
            verbose("\n\tSending x-path query for day: " + dayTranslator[day] )
            xpathQueryRes = driver.find_elements_by_xpath("/html/body/div[4]/div[2]/div/div/ul["+ str(day) + "]/li[contains(@class,'busy')]")
            for result in xpathQueryRes:
                dictOfEachDay[dayTranslator[day]].append(result.text) # adding each result in dict based on day
                verbose("\nx-path query response raw: \n" + result.text)                     
    except: #improve exception handling
        verbose("[WARNING] AN ERROR OCCURED WITH X-PATH QUERY. DAY: " + dayTranslator[day])


    driver.quit() # Important to quit !
    
    numberOfOrders = (sum([len(dictOfEachDay[x]) for x in dictOfEachDay if isinstance(dictOfEachDay[x], list)])) # checking if the lists are zero.

    # if numberOfOrders != 0: #Only contains the dicts and emtpy lists    
    #     verbose("\n\tCalling funciton: cleanRawQueryResponse(dictOfEachDay)\n")
    #     cleanedJSONFormat = cleanRawQueryResponse(dictOfEachDay)
    #     return cleanedJSONFormat

    #verbose("Did not find any orders in the given request/url")
    return dictOfEachDay
    #return None # No query results



#   NOT TESTED:
###################################
def findAvailableRooms(busyRooms):
    availableRooms = []
    dateTime = datetime.datetime.now()
    date = dateTime.date()
    dateStr = str(date)

    for result in busyRooms:
        if result['date'] == dateStr: # If the same date
            timeResult = result['time'] # Retrieve the time of result
            timeResultEnd = timeResult[-5:] # Get the end time => the last indexes
            timeObjEnd = datetime.datetime.strptime(timeResultEnd,'%H:%M') # format it to time-object
            if timeObjEnd.time() <= dateTime.time(): # if the room time is less than actual time (if the room is available)
                availableRooms.append(result.copy()) # copy the avaiable room to a list
    return availableRooms # return the result 
#####################################


# FIKSE DENNE FUNKJSON. ERROR NÅR DER ER BESTILT MANGE ROM PÅ EN BESTILLING
def prettyPrint(busyRooms):
    #date = ''
    dateTime = datetime.datetime.now()
    date = dateTime.date()
    date = str(date)
    for result in busyRooms:
        if date == result['date']:
            print("Date: ",result['date'],'\tRoom: ',result['room'],'\t\tTime: ', result['time'])
        


def main(argv):

    #soup=getWeekCalendar('https://tp.uio.no/ntnu/timeplan/?type=room&area%5B%5D=50000&building%5B%5D=501&id%5B%5D=501A146&week=12&ar=2020&stop=1')
    #findBusyRooms(soup)
    roomHelp = 'Specify room. To search for only classrooms, write \'classrooms\''
    url = 'https://tp.uio.no/ntnu/timeplan/?type=room&area%5B%5D=50000&building%5B%5D='
    buildings = {
        's':'510',
        'a':'501',
        'h':'505',
    }
    building = ''
    buildingURL = ''
    room = ''
    roomURL = ''

    #   Argument parser
    ########################
    parser = argparse.ArgumentParser()
    parser.add_argument('-b','--building', type=str,help = 'Specify building(s)'
                        ,required = True)
    parser.add_argument('-r','--room', type=str,help = roomHelp)
    parser.add_argument('-v','--verbose',dest="verbose",help='Turn verbosity on',default=False,action="store_true")
    args = parser.parse_args()
    ########################

    global VERBOSE 
    VERBOSE = args.verbose

    building = args.building.lower()

    # Finding building:
    if building == 's':
        buildingURL = buildings[building]
    elif building == 'a':
        buildingURL = buildings[building]
    elif building == 'h':
        buildingURL = buildings[building]
    elif building == 'e':
        buildingURL = buildings['h'] # 'e' is in 'h' building
    else:
        print("ERROR: Building not found..")
        print("Valid input: a, e, h, k, s")
        sys.exit()

    # Finding rooms
    if args.room is None:
        url = url + buildingURL + '&id[]=' + 'allRooms' + buildingURL
    else:
        room = args.room.upper() # Upper for the url
        
        if room == 'classrooms':
            print("Add classroom to url")
        else:
            roomURL = roomURL + room

        url = url + buildingURL + '&id[]=' + buildingURL + roomURL # '&id%5B%5D' på en -b A -r A162
    
    verbose('\n\tCalling function getAllBusyWeek(url)\n')
    result = getAllBusyWeek(url)
    print(result)
    #getAllBusyWeek('https://tp.uio.no/ntnu/timeplan/?type=room&area%5B%5D=50000&building%5B%5D=501&id%5B%5D=501A146&week=12&ar=2020&stop=1')







if __name__ == '__main__':
    main(sys.argv[1:])